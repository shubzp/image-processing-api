const amqp = require('amqplib/callback_api')
const config = require('../config')

const CONN_URL = config.InstanceConnectionURI
let ch = null;

const sendQueueOptions = {
   persistent : true
}

amqp.connect(CONN_URL, function (err, conn) {
   conn.createChannel(function (err, channel) {
      console.log(`Opening queue channel`);
      ch = channel;
   });
});

const publishToQueue = async (queueName, data) => {
   ch.sendToQueue(queueName, new Buffer.from(JSON.stringify(data)), sendQueueOptions);
}

process.on('exit', (code) => {
   ch.close();
   console.log(`Closing queue channel`);
});

module.exports = publishToQueue