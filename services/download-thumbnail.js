const path = require('path')
const fs = require('mz/fs')
const outputPath = './images/thumbnails/'

module.exports = {
    downloadThumbnailByImageName : (req, res) => {
        let outputImageName = 'thumb_' + req.params.imageName
        var fileLocation = getThumbnailLocation(outputImageName)
        fs.exists(fileLocation)
        .then((exists) => {
            if(exists) res.download(fileLocation, outputImageName);
            else res.status(200).send({message : "Requested file has no corresponding thumbnail."})
        })
        .catch((err) => {
            console.log(err)
        })
    }
}

getThumbnailLocation = (file) => {
    return path.join(outputPath,file)
}

