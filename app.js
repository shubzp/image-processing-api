const app = require('./index')
const config = require('./config')

let port = config.Port;

app.listen(port, (err) => {
    if(err) throw err
    console.log('Server started. Running on http://localhost:' + port)
})