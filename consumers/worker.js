const amqp = require('amqplib/callback_api');
const config = require('../config')
const thumbnailGenerator = require('./thumbnail-generator')

const queueName = config.QueueName
const CONN_URL = config.InstanceConnectionURI

const assertQueueOptions = {
  durable : true
}
const consumeQueueOptions = {
  noAck : false
}

amqp.connect(CONN_URL, function(err, conn) {
  if(err) throw err
  conn.createChannel(function(err, channel) {
    if(err) throw err
    
    channel.assertQueue(queueName, assertQueueOptions)
    channel.prefetch(1)
    
    console.log(" [*] Queue with name %s is ready for consumption", queueName)
    
    channel.consume(queueName, async function(msg) {
      console.log(" [x] Received %s", msg.content.toString());
      
      await thumbnailGenerator.processImage(msg, channel)
      
    }, consumeQueueOptions)
  });
});