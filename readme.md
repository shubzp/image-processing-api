## Thumbnail Generator API


Name : Shubham Panda

Email : shubhampanda126@gmail.com

Hello, there. 
Before running the service, kindly understand the architecture.

### Architecture
Since, image processing can be a time-consuming job, using a queue-based design is wise. For implementation purpose, I have designed a producer and a consumer. Queue used is RabbitMQ.

1. RabbitMQ : We will use QAAS (queue as a service model) provided by CloudAMQP where they offer use of RabbitMQ instances. This was chosen so that no additional setup would be required for queue. I have chosen ap-norteast-1 region which is closer to you guys for better performance. I have created a queue named <i>thumbnail-generation</i>. It is a simple queue with no additional settings enabled.

2. Producer : Our producer module will be used to enqueue messages. Broad level steps:
    - Using exposed end-point <i>Image Uploader</i>.
    - Selected image gets uploaded to a NAS location (here - images/uploads)
    - After uploading the file successfully, the message is published with the name of the image. (to keep queue payload size low)
    - Now our message is ready for consumption.

3. Consumer/s : Our consumer module will be used to dequeue and process messages. Broad level steps:
    - It is auto-triggered for consumption.
    - Once message is dequeued, we look up our NAS location for the file using image name received from the message.
    - The image will then undergo processing and a thumbnail will be created and stored in another NAS location (here : /images/thumbnails)
    - On success, acknowledgement is sent and hence message is purged from the queue.
    - On failure, no acknowledgement is sent and message is requeued.
    - Multiple consumers can perform the task concurrently for horizontal scaling.
4. Helper services : We have two services which are not in the architecture but can help from a user perspective. These services do not use connections to queues.
    - Process Image -- creates thumbnail for the concerned image.
    - Download Thumbnail -- downloads thumbnail for the concerned image.

We will use a connection string to connect to the remote RabbitMQ instance. This can be found in config.js file.

![Queue based model with RabbitMQ](/documentation/simplequeuewithrabbitmq.png)

### Scope
The implementation demonstrates how a queuing model can be applied for an image processing service.
Application of queues gives immense scalability for a time-consuming process. I have worked on something very similar in the past 2 years at my firm for document generation, reclaim processing, inbound file processing, etc. 

#### What I did not implement and why?
1. Multi-Upload using multer. - because accessor would have to search for multiple files to test the uploader functionality; uploading the same file multiple times can very well test the functionality of the queue.
2. Use of worker-threads. - because worker-threads or a thread pool would've complicated things as RabbitMQ channel is not thread safe. Worker-threads or a pool can be implemented using 'cluster' library which forks the primary server and distributes tasks to worker nodes.
3. Use of image identifier. - because it was easier with the image name and time-wise. If you upload an image with same name it will override the previous image. One possible solution for this is to use a storage mechanism to store key (identifier) and value image name. For physical file storage of images, we can append timestamp with the image name to avoid overriding of images.

#### Following improvements can be done on the current implementation:
1. Scalable - Consumers can be increased as per defined scaling policy. Queues can also be increased along with consumers.
2. DLQs - On failure there can be a retry threshold set up after which the messages will be dequeued from the original queue and sent to a dead letter queue. This helps in avoiding queue blockings. Moreover, the messages in the DLQs can be used to troubleshooting.
3. TTL - We can implement a TTL for every message. This will dequeue a certain message after the time exceeds expiration threshold.
4. Spliting/Sharding - If we implement a multiple queue architecture, we can use a queue cluster to allocate a queue to a node. This will allow us to keep our queues shorter by assigning a maximum items threshold to each queue. We can implement a logic to load balance the messages to be sent to the queue. This will help us keep track of the messages. For implementation, we can very well use a customized logic or use prebuilt plugins.
5. Batching - This is something that I have worked closely on. Consider a use case where multiple images are stored at one time using a multiple upload feature. Instead of publishing n messages to the queue for n images, we can batch all of them together in a single message and publish one message against all. Now, at the consumer end, after receiving the batched message, we can now implement worker threads to process images from the batch concurrently. This is a proven method of bringing down a process which once consumed 6 hours, down to ~43 mins. How to batch? There can be various logic. For eg. if we have predefined number of inputs each day, we can have a definite size (say 10), so for 10,000 inputs we publish only 1000 messges. But, in the use case of multiple image upload, we can batch all n items received at a time, in a single producer call. Next question in picture is payload. In most use cases that I have worked on, we use xml parsing because of multiple fields and heirarchy. In our case, we can simply put all image names in a list.

### Code
The code is modularised and has been enhanced for better readability.
We have two end-points:
1. Image Uploader (/uploadImages) which will upload a single image to the file and enqueue the message.
2. Image Processor (/processImage/<image_name>) which processes an image to create thumbnail. [This is not used by worker]
3. Download Thumbnail (/downloadThumbnail/<image_name>) which downloads thumbnail of corresponding image.

#### Image Upload
For uploading, multer library has been used. Multer library has been implemented to fetch and upload only a signle file. Filters have been added to allow only images. Filter has been applied on the mime type and not just the extension for better security.

#### Thumbnail Generation
For image processing, sharp library has been used. It provides quick resizing of images and also stores the file in the output directory.

#### RabbitMQ
We can always use rabbitmq image locally. However, we have used remote instance provided by CloudAMQP. A simple connection string is used to connect to the queue. For communicating with the queue, amqp library is used to establish connection to the channel and hence the exchange for access the queue both for publishing and consumption of messages. Library has build in methods like connect, consume, sendToQueue, etc.

#### Producer
Producer has been entitled the following tasks.
1. Image upload
2. Create a message payload with name and size of the uploaded image.
3. Publish message to queue.

#### Consumer
Consumer has been entitled the following tasks.
1. Dequeue a message.
2. Extract name of the image.
3. Read image from uploads folder.
4. Resize image
5. Store thumbnail in thumbnails folder.
6. Delete message from queue after processing completes, else re-queue.

### How to test the service

#### Container
1. We will have minimum two containers - producer and consumer. We can scale the consumer as desired to get better performance.
2. Container exposes the port 3000 and it binds to your machine's port 3000.
3. The NAS location mentioned - <i>images</i> will be mount to a volume named <i>assets</i> so that it can be shared by all the containers running.

#### Installation and Run
1. Running the service: <b>Recommended</b> 
- In your terminal fire command `docker-compose up --scale consumer=2`

    This will start a producer and two containers as shown in the image below.

    ![Recommended to run the service](/documentation/recommendedcommand.png)
- However, as requested in the assignment, a simple `docker-compose up` command would also work with one producer and one container.
- PS : Please ensure docker daemon is running.
2. Kindly import postman collection in your postman application. OR Use the curl urls given below:

    1. Image Uploader
    - This endpoint is responsible for uploading image and enqueueing. 
    - POST
    - http://localhost:3000/uploadImages
    - POSTMAN : Please select an image from the additional-assets section. The key name should be <i>imageName</i>.
    - CURL : 
    Get into the shell : `docker exec -it <containerid\> bash`
    Fire curl request : `curl -F imageName=@images/additional-assets/demotest-1.jpeg http://localhost:3000/uploadImages/`
    - Please check the images below:

        ![Image upload with postman](/documentation/postman-uploadimages.png)
        
        ![Image upload with curl](/documentation/curl-uploadimages.png)

    2. Process Image
    - Used to process a single image. Please use it for basic functionality testing of image processing.
    - GET
    - CURL `curl http://localhost:3000/processImage/image-1.jpeg`
    - Please refer the images below:

        ![Process image with postman](/documentation/postman-processimage.png)

        ![Process image with curl](/documentation/curl-processimage.png)
    
    3. Download Thumbnail
    - Used to download thumbnail of a given image.
    - GET
    - CURL : `curl http://localhost:3000/downloadThumbnail/image-1.jpeg --output thumbnail.jpeg`
    - Please refer the images below:

        ![Process image with postman](/documentation/postman-downloadthumbnail.png)

        ![Process image with curl](/documentation/curl-downloadthumbnail.png)

    4. PDF Test - Image Uploader
    - Illustration to show that any file other than image cannot be uploaded.
    - POSTMAN - Select demotest-6.pdf from images/additional-assets/ folder.
    - CURL - `curl -F imageName=@images/additional-assets/demotest-6.pdf http://localhost:3000/uploadImages/`

    
### Testing
We have used jest for writing unit and integration tests. Testing covers the following over 3 test files with 7 test cases:
1. Image Uploader Integration Test: This covers end to end testing of API (/uploadImages) along with file type check.

2. Thumbnail Generator Integration Test: This covers end to end testing of API (/processImage)

3. Thumbnail Generation Unit Test: This covers unit tests for our consumer module which is very vital for our processing of images.

#### Tools used testing
JEST : It is a famous testing tool for NodeJS and JS apps. Provides a wide variety of features including stubs, function-level mocking, module-level mocking, etc. Easy implementation is a plus.

#### Testing limitations
1. The only thing which I couldn't execute successfully was mocking. So, the folders and files that will be used for testing will be the original one's. This will not be a problem eventually because I am using a separate docker environment to test. However, an appropriate appsettings file for test could do the needful.
2. Testing makes actual connection to the queue, however, it doesn't enqueue messages because the concerned function has been mocked.

#### Run tests
Open a new terminal and fire the docker command :  `docker-compose -p tests run -p 3000 --rm producer npm test`

![Running tests](/documentation/testwithdockercommand.png)

### Production Readiness
1. Environment specific config files: 
    - We should ideally have an env_config folder consisting of files : dev_config.js, test_config.js, prod_config.js and one config.js as we already have in the structure.
    - By default config.js should have content from dev_config.js and that is the one that should be committed to git.
    - In our pipeline, during the deployment stage, we can write a script (eg. powershell script on windows servers) that would copy the appropriate config as per the server it is being uploaded too.
2. There must be an appsettings file which will store the NAS locations.
3. Setting up load-balancer.
4. Monitoring : Use of applications like Splunk, Geneos Alerts to get notified when there are spikes. Use an action command like queue purge, or stop taking new requests to the queue till the spike goes down below defined threshold. Enable health-checks.
5. Resiliency : If this is a client specific application, then we cannot avoid real-time results and will have to run the service 24*7. If it is to be used for internal processing required by another application, we can probably use a job scheduler like Autosys, cron jobs and schedule the job run at a time when the CPU usage is low and less users are active. We do this for 90% of our applications to manage appropriate CPU and memory usage.