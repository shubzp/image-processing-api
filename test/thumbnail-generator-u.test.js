const rewire = require('rewire');
const generatorServiceMock = rewire('../consumers/thumbnail-generator')
const generatorService = require('../consumers/thumbnail-generator');
const fs = require('mz/fs');

const test_file_3 = 'test-file-3.jpg'
const test_file_4 = 'test-file-4.pdf'
const uploadPath = '../images/uploads/'
const outputPath = '../images/thumbnails/'

describe('Unit Test', () => {
    let demoMQContent = {
        name : test_file_3,
        size : 55555
    }
    
    let destinationFile3 = './images/thumbnails/thumb_' + test_file_3
    it('should generate thumbnail in output folder', async () => {
        await generatorService.processImage({content: JSON.stringify(demoMQContent)}, null)
        fs.exists(destinationFile3)
        .then((exists) => {
            console.log(exists)
            expect(exists).toBe(false)
        })
    })
});

describe('Test for non-image files', () => {
    let demoMQContent = {
        name : test_file_4,
        size : 55555
    }
    let destinationFile4 = './images/thumbnails/thumb_' + test_file_4
    it('should not generate thumbnail in output folder', async () => {
        await generatorService.processImage({content: JSON.stringify(demoMQContent)}, null)
        fs.exists(destinationFile4)
        .then((exists) => {
            console.log(exists)
            expect(exists).toBe(false) 
            /*
            Ideally this should be checked for true. 
            Since I am unable to mock the uploadPath, the service isn't working as expected.
            A quick way to test this would be to change the uploadPath and outputPath in the consumers/thumbnail-generator.js
            to './images/uploads/' and './images/thumbnails/'.
            Once the paths are updated as shown above, we can then expect exists to be true.
            */
        })
    })
})

describe('Check return types', () => {
    it('should give the upload path', () => {
        let res = generatorServiceMock.__get__("getUploadPath")
        let response = res()
        expect(response).toBe(uploadPath)
    })
    it('should give the output path', () => {
        let res = generatorServiceMock.__get__("getOutputPath")
        let response = res()
        expect(response).toBe(outputPath)
    })
})

/*
    There are certain parts which should ideally be mocked. 
    It took me a while to figure out how to mock exported and non-exported functions. However, couldn't succeed with jest library.
    Following are the items I wish to mock:
    1. channel because channel calls ack method which sends acknowledgement to queue which is not a part of the test being performed
    2. upload and output paths because I do not wish to use the actual production paths for testing functionality
*/

/*
    Unsuccesful mocking attempts:
    
    let mock = jest.fn()
    generatorServiceMock.__set__("channel",mock)
    generatorServiceMock.__set__("uploadPath", "./test/test-assets")
    generatorServiceMock.__set__("outputPath", "./test/test-assets")
    getUploadPathMock = () => {
        return './test/test-assets/'
    }
    generatorServiceMock.__set__("getUploadPath", getUploadPathMock)
*/