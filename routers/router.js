const express = require('express')

const router = express.Router()

const thumbnail_generator_service = require('../services/thumbnail-generator')
const image_uploader_service = require('../producers/image-uploader')
const thumbnail_download_service = require('../services/download-thumbnail')

const upload = require('../helpers/multerUpload')

router.get('/processImage/:imageName', 
                    thumbnail_generator_service.processImage)
router.post('/uploadImages', 
                upload.single('imageName'),
                    image_uploader_service.uploadImages)
router.get('/downloadThumbnail/:imageName', 
                    thumbnail_download_service.downloadThumbnailByImageName)

module.exports = router