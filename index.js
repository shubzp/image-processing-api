const express = require('express')

const app = express()

app.use(express.urlencoded({extended : true}))
app.use(express.json())

const router = require('./routers/router')
app.use('/', router)
app.use('/', express.static('uploads'), router)

module.exports = app