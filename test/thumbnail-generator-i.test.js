const request = require('supertest');
const fs = require('mz/fs');
const app = require('../index');

const outputPath = './images/thumbnails/'

describe('Integration Test', () => {
  let test_file_1 = 'test-file-1.jpg'
  it('Processes an image from endpoint', (done) => {
    request(app)
      .get('/processImage/' + test_file_1)
      .end((err, res) => {
        expect(res.status).toBe(200)
        cleanup(test_file_1)
        done();
      });
  });

});

cleanup = (fileName) => {
  fs.unlink(outputPath + 'thumb_' + fileName, (err) => {
    if (err) {
      console.error(err)
      return
    }
  })
}

/*
About the test case.
Type : Integration test
What for: To check if the endpoint /processImages/image.jpg picks up the image from the 
          uploads folder, creates thumbnail and stores it into the thumbnails folder.
Procedure: Call the endpoint. Check if the status returned is 200.

In the test performed, the folders being used are the production folders.
The path should ideally be mocked and reference to the test/test-assets/ folder

Now since, the production paths are used, I have written a temporary method named cleanup
to delete the created files. Once mocking is successful, I will delete that method and 
do the allied changes.
*/

/*
Unsuccessful mocking attempts below:

const rewire = require('rewire');
const thumbnailGenerator1 = rewire('../producers/thumbnail-generator')
const thumbnailGenerator = require('../producers/thumbnail-generator')
let op = './test/test-assets/'

const mockedFunction = jest.spyOn(thumbnailGenerator,'getUploadPath')
mockedFunction.mockImplementation(() => {
  return op
})

const originalFn = thumbnailGenerator1.__get__('getUploadPath')
const mockedFunction = jest.spyOn(thumbnailGenerator1,originalFn)
mockedFunction.mockImplementation(() => {
  return op
})

mockedFunction.mockRestore()
*/