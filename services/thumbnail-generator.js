const sharp = require('sharp')
const uploadPath = './images/uploads/'
const outputPath = './images/thumbnails/'
const thumbnailHeight = 100
const thumbnailWidth = 100

module.exports = {
    processImage : (req, res) => {
        let inputImageName = req.params.imageName
        let outputImageName = 'thumb_' + inputImageName
        let status = {
            "isThumbnailGenerated" : false
        }
        let getPath = getUploadPath()
        let putPath = getOutputPath()

        sharp(getPath + inputImageName)
            .resize({
                width: thumbnailWidth,
                height: thumbnailHeight,
                fit: sharp.fit.cover,
                position: sharp.strategy.entropy
            })
            .sharpen()
            .toFile(putPath + outputImageName)
            .then(info => { 
                status.isThumbnailGenerated = true
                status.thumbnailInfo = info
                res.status(200).send(status)
            })
            .catch(err => {
                status.error = err
                console.log(err);
                res.status(200).send(status)
            });
    }
}

getUploadPath = () => {
    return uploadPath
}

getOutputPath = () => {
    return outputPath
}

