const sharp = require('sharp')
const mimeTypes = require('mime-types')
const uploadPath = '../images/uploads/'
const outputPath = '../images/thumbnails/'
const thumbnailHeight = 100
const thumbnailWidth = 100

module.exports = {
    processImage : async (msg, channel) => {
        let messageContent = JSON.parse(msg.content.toString())
        let inputImageName = messageContent.name
        let outputImageName = 'thumb_' + inputImageName
        let getPath = getUploadPath()
        let putPath = getOutputPath()
        let mime = mimeTypes.lookup(getPath + inputImageName)

        if(imageOnlyCheck(mime)){
            await resizeImage(getPath, inputImageName, putPath, outputImageName, channel, msg)
        }
    }
}

imageOnlyCheck = (mime) => {
    if(mime !== 'image/png' 
          && mime !== 'image/jpg' 
              && mime !== 'image/gif' 
                && mime !== 'image/jpeg') return false
    return true
}

getUploadPath = () => {
    return uploadPath
}

getOutputPath = () => {
    return outputPath
}

resizeImage = async (getPath, inputImageName, putPath, outputImageName, channel, msg) => {
    sharp(getPath + inputImageName)
        .resize({
            width: thumbnailWidth,
            height: thumbnailHeight,
            fit: sharp.fit.cover,
            position: sharp.strategy.entropy
        })
        .sharpen()
        .toFile(putPath + outputImageName)
        .then((info) => {
            console.log(' [x] Image processing completed. Thumbnail created is --> %s', outputImageName)
            channel.ack(msg);
        })
        .catch(err => {
            console.log(err);
            console.log(' [x] Processing failure. No acknowledgment sent. Message should be re-queued.')
        });
}