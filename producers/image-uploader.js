const publishToQueue = require('../producers/mqservice')
const config = require('../config')

module.exports = {
    uploadImages : async (req, res, next) => {
        const file = req.file;
        if (!file) {
            return res.status(400).send({ message: 'Please upload a file.' });
        }
        
        let queueName = config.QueueName
        let payload = {
            "name" : file.originalname,
            "size" : file.size
        }
        
        await publishToQueue(queueName, payload);
        return res.send({ message: 'File uploaded successfully.', file }
        );
    }
}