const request = require('supertest');
const fs = require('mz/fs');
const app = require('../index');

const test_file_1 = 'test-file-1.jpg'
const test_file_2 = 'test-file-2.pdf'
const filePath = `${__dirname}/test-assets/`;

const uploadPath = './images/uploads/'

jest.mock('../producers/mqservice')

describe('should upload a new image file', () => {
  let file1 = filePath + test_file_1;
  it('should upload the test file to upload directory', () => 
    fs.exists(file1)
      .then((exists) => {
        if (!exists) throw new Error('file does not exist'); 
        return request(app)
          .post('/uploadImages/')
          .attach('imageName', file1)
          .then((res) => {
            expect(res.statusCode).toBe(200);
            cleanup(test_file_1)
          })
          .catch(err => console.log(err));
      })
    )
  
  let file2 = filePath + test_file_2
  it('should throw error for a file other than image', () => 
    fs.exists(file2)
      .then((exists) => {
        if (!exists) throw new Error('file does not exist'); 
        return request(app)
          .post('/uploadImages/')
          .attach('imageName', file2)
          .then((res) => {
            expect(res.statusCode).toBe(500);
            cleanup(test_file_2)
          })
          .catch(err => console.log(err));
      })
    )
})

cleanup = (imageName) => {
  console.log(uploadPath + imageName)
  fs.unlink(uploadPath + imageName, (err) => {
    if (err) {
      console.error(err)
      return
    }
  })
}